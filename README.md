# It's called CORONA

Simple Firefox extension for replacing strings by `CORONA `on websites.

## Usage

In Firefox, open "about:debugging" and "Load Temporary Addon", switch to "This Firefox", then choose `manifest.json`. Enjoy.

Don't do this on browsers, where you are not allowed to!

## Licence

Code is released under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt), see [LICENCE.md](). Icons under `icons/` have been obtained from [https://openclipart.org]() and are used under a [CC-0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) licence.
